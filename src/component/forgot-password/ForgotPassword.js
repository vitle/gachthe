import React, { Component } from 'react';
import {Col, Row, Input, Button} from 'antd';
import * as CONST from './../../constant'

export default class ForgotPassword extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: '',
            email: '',
            code: '',
        }
    }
    render(){
        return(
            <div className="form-login">
                <Row>
                    <Col span={8} offset={8}>
                        <span>Tên đăng nhập</span>
                        <Input value={this.state.name} onChange={e => this.onChange('name',e.target.value)} />
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Email</span>
                        <Input value={this.state.email}  onChange={e => this.onChange('email',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Mã bảo vệ</span>
                        <Input value={this.state.code}  onChange={e => this.onChange('code',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <Button type="primary" className="btn-card" onClick={this.onRegister}>Đổi mật khẩu</Button>
                    </Col>
                </Row>
            </div>
        )
    }

    onChange = (name, value)=>{
        this.setState({
            [name]: value
        })
    }

    onRegister = () =>{

    }
}