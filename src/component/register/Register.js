import React, { Component } from 'react';
import {Col, Row, Input, Button, Radio} from 'antd';
import * as CONST from './../../constant'
import './register.css'

const RadioGroup = Radio.Group;
export default class Register extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: '',
            email: '',
            password: '',
            code: '',
            accept: 0
        }
    }
    render(){
        return(
            <div className="form-login">
                <Row>
                    <Col span={8} offset={8}>
                        <span>Tên đăng nhập</span>
                        <Input value={this.state.name} onChange={e => this.onChange('name',e.target.value)} />
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Email</span>
                        <Input value={this.state.email}  onChange={e => this.onChange('email',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Mật khẩu</span>
                        <Input value={this.state.password}  onChange={e => this.onChange('password',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Mã bảo vệ</span>
                        <Input value={this.state.code}  onChange={e => this.onChange('code',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <RadioGroup onChange={e => this.onChange('accept',e.target.value)} value={this.state.accept}>
                            <Radio value={1}>Tôi đồng ý với các quy định của hệ thống.</Radio>
                        </RadioGroup>
                    </Col>
                    <Col span={8} offset={8}>
                        <Button type="primary" className="btn-card" onClick={this.onRegister}>Đăng ký</Button>
                    </Col>
                </Row>
            </div>
        )
    }

    onChange = (name, value)=>{
        this.setState({
            [name]: value
        })
    }

    onRegister = () =>{

    }
}