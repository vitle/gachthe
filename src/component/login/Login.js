import React, { Component } from 'react';
import {Col, Row, Input, Button} from 'antd';
import * as CONST from './../../constant'
import './login.css'

export default class Login extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: '',
            password: ''
        }
    }
    render(){
        return(
            <div className="form-login">
                <Row>
                    <Col span={8} offset={8}>
                        <span>Tên đăng nhập</span>
                        <Input value={this.state.name} onChange={e => this.onChange('name',e.target.value)} />
                    </Col>
                    <Col span={8} offset={8}>
                        <span>Mật khẩu</span>
                        <Input value={this.state.password}  onChange={e => this.onChange('password',e.target.value)}/>
                    </Col>
                    <Col span={8} offset={8}>
                        <Button type="primary" className="btn-card" onClick={this.onLogin}>Đăng nhập</Button>
                        <Button type="primary" className="btn-card" onClick={this.onForgot}>Quên mật khẩu</Button>
                    </Col>
                </Row>
            </div>
        )
    }

    onChange = (name, value)=>{
        this.setState({
            [name]: value
        })
    }

    onLogin = () =>{

    }

    onForgot = () => {

    }
}