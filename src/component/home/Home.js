import React, { Component } from 'react';
import {Col, Row} from 'antd';
import * as CONST from './../../constant'
import ConvertCard from './component/ConvertCard'
import BuyCard from './component/BuyCard'
import './Home.css'

export default class Home extends Component {
    render(){
        return (
            <div>
                <Row gutter={24}>
                    <Col span={10} offset={2}>
                        <ConvertCard />
                    </Col>
                    <Col span={10}>
                        <BuyCard />
                    </Col>
                </Row>
                <Row className="convert-box">
                    <Col span ={20} offset={2} className="title">
                        <h2>CHIẾT KHẤU ĐỔI MÃ THẺ THÀNH TIỀN</h2>
                    </Col>
                    <Col span ={20} offset={2}>
                        {this.renderBox(CONST.CARD_DISCOUNT_CONVERT)}
                    </Col>
                </Row>
                <Row className="buy-box">
                    <Col span ={20} offset={2} className="title">
                        <h2>CHIẾT KHẤU MUA MÃ THẺ CÀO</h2>
                    </Col>
                    <Col span ={20} offset={2}>
                        {this.renderBox(CONST.CARD_DISCOUNT_BUY)}
                    </Col>
                </Row>
            </div>
        )
    }

    renderBox = (data) =>{
        return data.map((value, i) =>{
            return (
                <Col span={6} className="home-box" key = {i}>
                    <Col span={24}>{value.name}</Col>
                    <Col span={24}>
                        <img src={value.icon}/>
                    </Col>
                    <Col span={24}>{value.value}</Col>
                </Col>
            )
        })
    }
}


