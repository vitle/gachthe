import React, { Component } from 'react';
import {Col, Row, Select, Input, Button} from 'antd';
import * as CONST from './../../../constant';

let Option = Select.Option;

export default class BuyCard extends Component {
    constructor(props){
        super(props)

        this.state = {
            card_type: 1,
            card_value: 50000,
            phone: '',
            quantity: '',
            codeRandom : ''
        }
    }
    render(){
        let {card_type, card_value, phone, quantity, codeRandom} = this.state
        return(
            <div className="buy-card">
                <Row>
                    <Col span={24} className="card-title">
                        <h1>Đổi thẻ thành tiền</h1>
                    </Col>
                    <Row className="card-content">
                        <Col span={24}>
                            Hãy đăng nhập để nạp!
                        </Col>
                        <Col span={24}>
                            <span>Loại thẻ: </span>
                            <Select  defaultValue={card_type} style={{ width: "100%" }} onChange={value => this.onChange('card_type', value)}>
                                {this.getOptions(CONST.CARD_TYPE)}
                            </Select>
                        </Col>
                        <Col span={24}>
                            <span>Mệnh giá: </span>
                            <Select  defaultValue={card_value} style={{ width: "100%" }} onChange={value => this.onChange('card_number', value)}>
                                {this.getOptions(CONST.CARD_VALUE)}
                            </Select>
                        </Col>
                        <Col span={24}>
                            <span>Số điện thoại liên hệ: </span>
                            <Input onChange={e => this.onChange('phone',e.target.value)} value={phone}/>
                        </Col>
                        <Col span={24}>
                            <span>Số lượng cần mua: </span>
                            <Input onChange={e => this.onChange('quantity',e.target.value)} value={quantity}/>
                        </Col>
                        <Col span={24}>
                            <span>Mã bảo vệ: </span>
                            <Input onChange={e => this.onChange('codeRandom',e.target.value)} value={codeRandom}/>
                        </Col>
                        <Col span={24}>
                            <Button  type="primary" className="btn-card" onClick={this.onClick}>Gửi yêu cầu</Button>
                        </Col>
                    </Row>
                </Row>
            </div>
        )
    }

    onChange = (name,value) =>{
        this.setState({
            [name]: value
        })
    }

    getOptions = (data) =>{
        let options = []
        for(let i in data){
            options.push(
                <Option key={i} value={i} >{data[i]}</Option>
            )
        }
        return options
    }

    onClick = () => {
        
    }
}