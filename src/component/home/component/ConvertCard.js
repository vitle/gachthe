import React, { Component } from 'react';
import {Col, Row, Select, Input, Button} from 'antd';
import * as CONST from './../../../constant';
import './style.css'

let Option = Select.Option;

export default class ConvertCard extends Component {
    constructor(props){
        super(props)

        this.state = {
            card_type: 1,
            card_value: 50000,
            code: '',
            seri: '',
            codeRandom : ''
        }
    }
    render(){
        let {card_type, card_value, code, seri, codeRandom} = this.state
        return(
            <div className="convert-card">
                <Row>
                    <Col span={24} className="card-title">
                        <h1>Đổi thẻ thành tiền</h1>
                    </Col>
                    <Row className="card-content">
                        <Col span={24}>
                            Hãy đăng nhập để nạp!
                        </Col>
                        <Col span={24}>
                            <span>Loại thẻ: </span>
                            <Select  defaultValue={card_type} style={{ width: "100%" }} onChange={value => this.onChange('card_type', value)}>
                                {this.getOptions(CONST.CARD_TYPE)}
                            </Select>
                        </Col>
                        <Col span={24}>
                            <span>Mệnh giá: </span>
                            <Select  defaultValue={card_value} style={{ width: "100%" }} onChange={value => this.onChange('card_number', value)}>
                                {this.getOptions(CONST.CARD_VALUE)}
                            </Select>
                        </Col>
                        <Col span={24}>
                            <span>Mã thẻ: </span>
                            <Input onChange={e => this.onChange('code',e.target.value)} value={code}/>
                        </Col>
                        <Col span={24}>
                            <span>Số seri: </span>
                            <Input onChange={e => this.onChange('seri',e.target.value)} value={seri}/>
                        </Col>
                        <Col span={24}>
                            <span>Mã bảo vệ: </span>
                            <Input onChange={e => this.onChange('codeRandom',e.target.value)} value={codeRandom}/>
                        </Col>
                        <Col span={24}>
                            <Button type="primary" className="btn-card" onClick={this.onClick}>Nạp tiền</Button>
                        </Col>
                    </Row>
                </Row>
            </div>
        )
    }

    onChange = (name,value) =>{
        this.setState({
            [name]: value
        })
    }

    getOptions = (data) =>{
        let options = []
        for(let i in data){
            options.push(
                <Option key={i} value={i} >{data[i]}</Option>
            )
        }
        return options
    }

    onClick = () => {
        
    }
}