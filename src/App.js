import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import RouterApp from './router'
import Header from './layout/Header'
import MenuApp from './layout/Menu'


class App extends Component {
  render() {
    return (
      <div>
              <Header />
              <MenuApp />
              <RouterApp />
      </div>
    );
  }
}

export default App;
