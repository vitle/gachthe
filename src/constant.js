export const CARD_TYPE = {
    1 : 'Viettel',
    2 : 'Vinaphone',
    3 : 'Mobifone',
    4 : 'Zing',
    5 : 'Vcoin',
    6 : 'Garena'
}

export const CARD_VALUE = {
    50000 : '50.000đ',
    100000 : '100.000đ',
    200000 : '200.000đ',
    500000 : '500.000đ'
}

export const CARD_DISCOUNT_CONVERT = [
    {
        name: 'Viettel',
        value: '37%',
        icon: 'http://gachthe247.com/images/cards/viettel.png'
    },
    {
        name: 'Vinaphone',
        value: '37%',
        icon: 'http://gachthe247.com/images/cards/vinaphone.png'
    },
    {
        name: 'Mobifone',
        value: '35%',
        icon: 'http://gachthe247.com/images/cards/mobiphone.png'
    },
    {
        name: 'Zing',
        value: '40%',
        icon: 'http://gachthe247.com/images/cards/zing.png'
    },
    {
        name: 'Vcoint',
        value: '40%',
        icon: 'http://gachthe247.com/images/cards/vcoin.jpg'
    },
    {
        name: 'Garena',
        value: '30%',
        icon: 'http://gachthe247.com/images/cards/garena.png'
    }
]

export const CARD_DISCOUNT_BUY = [
    {
        name: 'Viettel',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/viettel.png'
    },
    {
        name: 'Vinaphone',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/vinaphone.png'
    },
    {
        name: 'Mobifone',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/mobiphone.png'
    },
    {
        name: 'Zing',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/zing.png'
    },
    {
        name: 'Vcoint',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/vcoin.jpg'
    },
    {
        name: 'Garena',
        value: '15%',
        icon: 'http://gachthe247.com/images/cards/garena.png'
    }
]