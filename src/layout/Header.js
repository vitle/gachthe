import React, { Component } from 'react';
import {Col, Row} from 'antd';
import 'antd/dist/antd.css';
import './style.css'

export default class Header extends Component {
    render(){
        return(
           <div>
               <Row className= "header-content">
                   <Col span={6} offset={2}>
                        <img src="http://gachthe247.com/images/icon/logo.png" />
                    </Col>
                    <Col span={4} offset={6}>
                    <div className ="head-text">
                        <ul>
                            <li>
                                <a href="/" title="Sáng: 8h00 - 11h30">
                                    Sáng: 8h00 - 11h30				</a>
                            </li>
                                    <li>
                                <a href="/" title="Chiều: 13h30 - 22h30">
                                    Chiều: 13h00 - 17h30				</a>
                            </li>
                                    <li>
                                <a href="/" title="Chiều: 13h30 - 22h30">
                                Tối: 19h00 - 22h30				</a>
                            </li>
                        </ul>
                    </div>
                    </Col>
                    <Col span={4}>
                        <div className="head-hotline">
                            <ul>
                                <li><span className="icon"><img src="http://gachthe247.com/images/icon/h-phone.png" /></span> <span>0164.357.3456</span></li>
                                
                                <li><span className="icon"><img src="http://gachthe247.com/images/icon/h-mobile.png" /></span> <span>0164.357.3456</span></li>
                                
                                <li><span className="icon"><img src="http://gachthe247.com/images/icon/h-mail.png" /></span> <span>Gachthe247.com</span></li>
                            </ul>
                        </div>
                    </Col>
               </Row>
           </div> 
        )
    }

}