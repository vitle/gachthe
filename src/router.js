import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import {Row, Col, Menu, Icon } from 'antd';
import Home from './component/home/Home'
import Payment from './component/payment/Payment'
import Login from './component/login/Login'
import Register from './component/register/Register'
import ForgotPassword from './component/forgot-password/ForgotPassword'



export default class RouterApp extends Component {
    constructor(props){
        super(props)

        this.state = {
            current: 'home',
        }
    }
    render(){
        return(
            <Router>
            <div>
                <Row>
                    <Col span="22" offset="2">
                        <Menu
                            onClick={this.handleClick}
                            selectedKeys={[this.state.current]}
                            mode="horizontal">

                            <Menu.Item key="home">
                                <Link to="/"><Icon type="home" />Trang chủ</Link>
                            </Menu.Item>

                            <Menu.Item key="payment">
                                <Link to="/payment"><Icon type="money" />Rút tiền</Link>
                            </Menu.Item>

                            <Menu.Item key="login">
                                <Link to="/login"><Icon type="user" />Đăng nhập</Link>
                            </Menu.Item>

                            <Menu.Item key="register">
                                <Link to="/register"><Icon type="key" />Đăng kí</Link>
                            </Menu.Item>

                            <Menu.Item key="forgotpassword">
                                <Link to="/forgotpassword"><Icon type="password" />Quên mật khẩu</Link>
                            </Menu.Item>
                        </Menu>
                    </Col>
                </Row>
               <Switch>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/payment' component={Payment} />
                  <Route exact path='/login' component={Login} />
                  <Route exact path='/register' component={Register} />
                  <Route exact path='/forgotpassword' component={ForgotPassword} />
               </Switch>
            </div>
         </Router>
        )
    }

    handleClick = (e) => {
        this.setState({
          current: e.key,
        });
      }
}